#if 0
#pragma makedep unix
#endif

#include <assert.h>

#include <capstone/capstone.h>

#include "winternl.h"
#include "unix_private.h"
#include "wine/debug.h"
#include "wine/asm.h"

WINE_DEFAULT_DEBUG_CHANNEL(x86emu);

#if defined(__i386__)
#define ARCH_RIP X86_REG_EIP
#define ARCH_RAX X86_REG_EAX
#define ARCH_RBX X86_REG_EBX
#define ARCH_RCX X86_REG_ECX
#define ARCH_RDX X86_REG_EDX
#define ARCH_RSP X86_REG_ESP
#define ARCH_RBP X86_REG_EBP
#define ARCH_RSI X86_REG_ESI
#define ARCH_RDI X86_REG_EDI
#elif defined(__x86_64)
#define ARCH_RIP X86_REG_RIP
#define ARCH_RAX X86_REG_RAX
#define ARCH_RBX X86_REG_RBX
#define ARCH_RCX X86_REG_RCX
#define ARCH_RDX X86_REG_RDX
#define ARCH_RSP X86_REG_RSP
#define ARCH_RBP X86_REG_RBP
#define ARCH_RSI X86_REG_RSI
#define ARCH_RDI X86_REG_RDI
#endif

#if 1
//#if defined(__x86_64__)

static size_t get_register_size(x86_reg reg, x86_reg *base_reg)
{
    switch (reg)
    {
        case ARCH_RIP: *base_reg = ARCH_RIP; return sizeof(void*);
        case X86_REG_EFLAGS: *base_reg = X86_REG_EFLAGS; return 4;

#ifdef __x86_64__
        case X86_REG_RAX: *base_reg = X86_REG_RAX; return 8;
        case X86_REG_RBX: *base_reg = X86_REG_RBX; return 8;
        case X86_REG_RCX: *base_reg = X86_REG_RCX; return 8;
        case X86_REG_RDX: *base_reg = X86_REG_RDX; return 8;
        case X86_REG_RSP: *base_reg = X86_REG_RSP; return 8;
        case X86_REG_RBP: *base_reg = X86_REG_RBP; return 8;
        case X86_REG_RSI: *base_reg = X86_REG_RSI; return 8;
        case X86_REG_RDI: *base_reg = X86_REG_RDI; return 8;
        case X86_REG_R8: *base_reg = X86_REG_R8; return 8;
        case X86_REG_R9: *base_reg = X86_REG_R9; return 8;
        case X86_REG_R10: *base_reg = X86_REG_R10; return 8;
        case X86_REG_R11: *base_reg = X86_REG_R11; return 8;
        case X86_REG_R12: *base_reg = X86_REG_R12; return 8;
        case X86_REG_R13: *base_reg = X86_REG_R13; return 8;
        case X86_REG_R14: *base_reg = X86_REG_R14; return 8;
        case X86_REG_R15: *base_reg = X86_REG_R15; return 8;
#endif

        case X86_REG_EAX: *base_reg = ARCH_RAX; return 4;
        case X86_REG_EBX: *base_reg = ARCH_RBX; return 4;
        case X86_REG_ECX: *base_reg = ARCH_RCX; return 4;
        case X86_REG_EDX: *base_reg = ARCH_RDX; return 4;
        case X86_REG_ESP: *base_reg = ARCH_RSP; return 4;
        case X86_REG_EBP: *base_reg = ARCH_RBP; return 4;
        case X86_REG_ESI: *base_reg = ARCH_RSI; return 4;
        case X86_REG_EDI: *base_reg = ARCH_RDI; return 4;
#ifdef __x86_64__
        case X86_REG_R8D: *base_reg = X86_REG_R8; return 4;
        case X86_REG_R9D: *base_reg = X86_REG_R9; return 4;
        case X86_REG_R10D: *base_reg = X86_REG_R10; return 4;
        case X86_REG_R11D: *base_reg = X86_REG_R11; return 4;
        case X86_REG_R12D: *base_reg = X86_REG_R12; return 4;
        case X86_REG_R13D: *base_reg = X86_REG_R13; return 4;
        case X86_REG_R14D: *base_reg = X86_REG_R14; return 4;
        case X86_REG_R15D: *base_reg = X86_REG_R15; return 4;
#endif

        case X86_REG_AX: *base_reg = ARCH_RAX; return 2;
        case X86_REG_BX: *base_reg = ARCH_RBX; return 2;
        case X86_REG_CX: *base_reg = ARCH_RCX; return 2;
        case X86_REG_DX: *base_reg = ARCH_RDX; return 2;
        case X86_REG_SP: *base_reg = ARCH_RSP; return 2;
        case X86_REG_BP: *base_reg = ARCH_RBP; return 2;
        case X86_REG_SI: *base_reg = ARCH_RSI; return 2;
        case X86_REG_DI: *base_reg = ARCH_RDI; return 2;
#ifdef __x86_64__
        case X86_REG_R8W: *base_reg = X86_REG_R8; return 2;
        case X86_REG_R9W: *base_reg = X86_REG_R9; return 2;
        case X86_REG_R10W: *base_reg = X86_REG_R10; return 2;
        case X86_REG_R11W: *base_reg = X86_REG_R11; return 2;
        case X86_REG_R12W: *base_reg = X86_REG_R12; return 2;
        case X86_REG_R13W: *base_reg = X86_REG_R13; return 2;
        case X86_REG_R14W: *base_reg = X86_REG_R14; return 2;
        case X86_REG_R15W: *base_reg = X86_REG_R15; return 2;
#endif

        case X86_REG_AL: *base_reg = ARCH_RAX; return 1;
        case X86_REG_BL: *base_reg = ARCH_RBX; return 1;
        case X86_REG_CL: *base_reg = ARCH_RCX; return 1;
        case X86_REG_DL: *base_reg = ARCH_RDX; return 1;
        case X86_REG_SPL: *base_reg = ARCH_RSP; return 1;
        case X86_REG_BPL: *base_reg = ARCH_RBP; return 1;
        case X86_REG_SIL: *base_reg = ARCH_RSI; return 1;
        case X86_REG_DIL: *base_reg = ARCH_RDI; return 1;
#ifdef __x86_64__
        case X86_REG_R8B: *base_reg = X86_REG_R8; return 1;
        case X86_REG_R9B: *base_reg = X86_REG_R9; return 1;
        case X86_REG_R10B: *base_reg = X86_REG_R10; return 1;
        case X86_REG_R11B: *base_reg = X86_REG_R11; return 1;
        case X86_REG_R12B: *base_reg = X86_REG_R12; return 1;
        case X86_REG_R13B: *base_reg = X86_REG_R13; return 1;
        case X86_REG_R14B: *base_reg = X86_REG_R14; return 1;
        case X86_REG_R15B: *base_reg = X86_REG_R15; return 1;
#endif

#ifdef __x86_64__
        case X86_REG_XMM0: *base_reg = X86_REG_XMM0; return 16;
        case X86_REG_XMM1: *base_reg = X86_REG_XMM1; return 16;
        case X86_REG_XMM2: *base_reg = X86_REG_XMM2; return 16;
        case X86_REG_XMM3: *base_reg = X86_REG_XMM3; return 16;
        case X86_REG_XMM4: *base_reg = X86_REG_XMM4; return 16;
        case X86_REG_XMM5: *base_reg = X86_REG_XMM5; return 16;
        case X86_REG_XMM6: *base_reg = X86_REG_XMM6; return 16;
        case X86_REG_XMM7: *base_reg = X86_REG_XMM7; return 16;
        case X86_REG_XMM8: *base_reg = X86_REG_XMM8; return 16;
        case X86_REG_XMM9: *base_reg = X86_REG_XMM9; return 16;
        case X86_REG_XMM10: *base_reg = X86_REG_XMM10; return 16;
        case X86_REG_XMM11: *base_reg = X86_REG_XMM11; return 16;
        case X86_REG_XMM12: *base_reg = X86_REG_XMM12; return 16;
        case X86_REG_XMM13: *base_reg = X86_REG_XMM13; return 16;
        case X86_REG_XMM14: *base_reg = X86_REG_XMM14; return 16;
        case X86_REG_XMM15: *base_reg = X86_REG_XMM15; return 16;
#endif

        default: return 0;
    }
}

int get_register_addr_ucontext(ucontext_t *ctx, x86_reg reg, void **value);

static int get_register_addr_impl(struct x86emu_ctx *ctx, x86_reg reg, void **value, size_t *size)
{
    x86_reg base_reg;
    *size = get_register_size(reg, &base_reg);
    if (*size == 0)
        return 0;
    if (ctx->ctx)
        return get_register_addr_ucontext(ctx->ctx, base_reg, value);
    else
        return 0;
}

static int get_register_addr(struct x86emu_ctx *ctx, x86_reg reg, void **value)
{
    size_t size;
    return get_register_addr_impl(ctx, reg, value, &size);
}

static int get_register_value(struct x86emu_ctx *ctx, x86_reg reg, uint64_t *value)
{
    size_t size;
    void *addr;
    if (!get_register_addr_impl(ctx, reg, &addr, &size))
        return 0;
    if (size > 8)
        return 0;
    *value = 0;
    memcpy(value, addr, size);
    return 1;
}

static int set_register_value(struct x86emu_ctx *ctx, x86_reg reg, uint64_t value)
{
    size_t size;
    void *addr;
    if (!get_register_addr_impl(ctx, reg, &addr, &size))
        return 0;
    if (size > 8)
        return 0;
    memcpy(addr, &value, size);
    return 1;
}

static void *safe_get_register_addr(struct x86emu_ctx *ctx, x86_reg reg)
{
    void *addr;
    int res = get_register_addr(ctx, reg, &addr);
    assert(res);
    return addr;
}

static uint64_t safe_get_register_value(struct x86emu_ctx *ctx, x86_reg reg)
{
    uint64_t value;
    int res = get_register_value(ctx, reg, &value);
    assert(res);
    return value;
}

static void safe_set_register_value(struct x86emu_ctx *ctx, x86_reg reg, uint64_t value)
{
    int res = set_register_value(ctx, reg, value);
    assert(res);
}

static uint64_t get_fs_base(struct x86emu_ctx *ctx)
{
    uintptr_t base;
    __asm__ volatile("rdfsbase %0\n" : "=r"(base));
    return base;
}

static uint64_t get_gs_base(struct x86emu_ctx *ctx)
{
    uintptr_t base;
    __asm__ volatile("rdgsbase %0\n" : "=r"(base));
    return base;
}

static int get_memory_addr(struct x86emu_ctx *ctx, const x86_op_mem *op, void **value, int with_segment)
{
    uint64_t tmp;
    uint64_t addr = op->disp;
    if (op->base != X86_REG_INVALID)
    {
        if (get_register_value(ctx, op->base, &tmp))
            addr += tmp;
        else
            return 0;
    }
    if (op->index != X86_REG_INVALID)
    {
        if (get_register_value(ctx, op->index, &tmp))
            addr += op->scale * tmp;
        else return 0;
    }
    if (with_segment)
    {
        if (op->segment == X86_REG_FS)
            addr += get_fs_base(ctx);
        else if (op->segment == X86_REG_GS)
            addr += get_gs_base(ctx);
        else if (op->segment != X86_REG_INVALID)
            return 0;
    }
    *value = (void*)(uintptr_t)addr;
    return 1;
}

static int get_mem(void *addr, uint64_t *value, uint8_t size)
{
    if (addr == (void*)0x7ffe0308)
    {
        TRACE("intercept syscall gadget\n");
        return 0;
    }

    switch (size)
    {
        case 1: *value = *(uint8_t*)addr; return 1;
        case 2: *value = *(uint16_t*)addr; return 1;
        case 4: *value = *(uint32_t*)addr; return 1;
        case 8: *value = *(uint64_t*)addr; return 1;
        default: return 0;
    }
}

static int set_mem(void *addr, uint64_t value, uint8_t size)
{
    switch (size)
    {
        case 1: *(uint8_t*)addr = value; return 1;
        case 2: *(uint16_t*)addr = value; return 1;
        case 4: *(uint32_t*)addr = value; return 1;
        case 8: *(uint64_t*)addr = value; return 1;
        default: return 0;
    }
}

static int set_mem_lock(void *addr, uint64_t value, uint64_t old_value, uint8_t size)
{
    switch (size)
    {
        case 1: return __atomic_compare_exchange((uint8_t*)addr, (uint8_t*)&old_value, (uint8_t*)&value, 1, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
        case 2: return __atomic_compare_exchange((uint16_t*)addr, (uint16_t*)&old_value, (uint16_t*)&value, 1, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
        case 4: return __atomic_compare_exchange((uint32_t*)addr, (uint32_t*)&old_value, (uint32_t*)&value, 1, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
        case 8: return __atomic_compare_exchange((uint64_t*)addr, (uint64_t*)&old_value, (uint64_t*)&value, 1, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
        default: return 0;
    }
}

static int get_operand_addr(struct x86emu_ctx *ctx, const cs_x86_op *op, void **value, int with_segment)
{
    switch (op->type)
    {
        case X86_OP_REG: return get_register_addr(ctx, op->reg, value);
        case X86_OP_IMM: return 0;
        case X86_OP_MEM: return get_memory_addr(ctx, &op->mem, value, with_segment);
        default: return 0;
    }
}

#ifdef __x86_64__
static void read_helper(void *addr, void *value, uint8_t size)
{
    uint8_t i;

    memcpy(value, addr, size);

    TRACE("reading from memory value ");
    for (i = 0; i < size; i++)
    {
        TRACE("%02x", (unsigned int)((unsigned char*)value)[size - i - 1]);
    }
    TRACE("\n");
}

static int is_operand_memory(const cs_x86_op *op)
{
    return op->type == X86_OP_MEM;
}

static int is_operand_xmm_register(const cs_x86_op *op)
{
    return op->type == X86_OP_REG && op->reg >= X86_REG_XMM0 && op->reg <= X86_REG_XMM15;
}
#endif

static int get_operand_value(struct x86emu_ctx *ctx, const cs_x86_op *op, uint64_t *value)
{
    void *addr;

    switch (op->type)
    {
        case X86_OP_IMM:
            *value = op->imm;
            return 1;

        case X86_OP_REG:
        case X86_OP_MEM:
            if (!get_operand_addr(ctx, op, &addr, 1))
                return 0;
            if (!get_mem(addr, value, op->size))
                return 0;
            if (op->type == X86_OP_MEM)
            {
                uint8_t i;

                TRACE("reading from memory value ");
                for (i = 0; i < op->size; i++)
                {
                    TRACE("%02x", (unsigned int)((unsigned char*)value)[op->size - i - 1]);
                }
                TRACE("\n");
            }
            return 1;

        default:
            return 0;
    }
}

static int set_operand_value(struct x86emu_ctx *ctx, const cs_x86_op *op, uint64_t value, const uint64_t *prev_value)
{
    void *addr;

    switch (op->type)
    {
        case X86_OP_IMM:
            return 0;

        case X86_OP_REG:
        case X86_OP_MEM:
            if (!get_operand_addr(ctx, op, &addr, 1))
                return 0;
            if (prev_value)
                return set_mem_lock(addr, value, *prev_value, op->size);
            else
            {
#ifdef __x86_64__
                if (op->type == X86_OP_REG && op->size == 4)
                    return set_mem(addr, (uint32_t)value, 8);
                else
#endif
                    return set_mem(addr, value, op->size);
            }

        default:
            return 0;
    }
}

#if defined(__x86_64__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $8, %5\n"                                     \
                     "je 1f\n"                                          \
                     "cmp $4, %5\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %5\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %5\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "1: " #op " %%rcx, %%rax\n"                        \
                     "jmp 5f\n"                                         \
                     "2: " #op " %%ecx, %%eax\n"                        \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%cx, %%ax\n"                          \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%cl, %%al\n"                          \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*flags), "=a"(*x), "=r"(ret)                \
                     : "a"(*x), "c"(*y), "r"(size), "2"(ret));          \
    return ret;
#elif defined(__i386__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $4, %5\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %5\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %5\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "2: " #op " %%ecx, %%eax\n"                        \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%cx, %%ax\n"                          \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%cl, %%al\n"                          \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*(uint32_t*)flags), "=a"(*(uint32_t*)x), "=r"(ret) \
                     : "a"(*(uint32_t*)x), "c"(*(uint32_t*)y), "r"((uint32_t)size), "2"(ret)); \
    return ret;
#endif

static int binary_op_with_flags(uint64_t *x, uint64_t *y, unsigned int op, uint8_t size, uint64_t *flags)
{
    int ret = 1;
    switch (op)
    {
        HANDLE_OP(ADD)
        HANDLE_OP(SUB)
        HANDLE_OP(ADC)
        HANDLE_OP(SBB)
        HANDLE_OP(XOR)
        HANDLE_OP(OR)
        HANDLE_OP(AND)
        HANDLE_OP(CMP)
        HANDLE_OP(TEST)
    default: return 0;
    }
}

#undef HANDLE_OP

#if defined(__x86_64__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $8, %5\n"                                     \
                     "je 1f\n"                                          \
                     "cmp $4, %5\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %5\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %5\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "1: " #op " %%cl, %%rax\n"                         \
                     "jmp 5f\n"                                         \
                     "2: " #op " %%cl, %%eax\n"                         \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%cl, %%ax\n"                          \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%cl, %%al\n"                          \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*flags), "=a"(*x), "=r"(ret)                \
                     : "a"(*x), "c"(*y), "r"(size), "2"(ret));          \
    return ret;
#elif defined(__i386__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $4, %5\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %5\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %5\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "2: " #op " %%cl, %%eax\n"                         \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%cl, %%ax\n"                          \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%cl, %%al\n"                          \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*(uint32_t*)flags), "=a"(*(uint32_t*)x), "=r"(ret) \
                     : "a"(*(uint32_t*)x), "c"(*(uint32_t*)y), "r"((uint32_t)size), "2"(ret)); \
    return ret;
#endif

static int shift_with_flags(uint64_t *x, uint64_t *y, unsigned int op, uint8_t size, uint64_t *flags)
{
    int ret = 1;
    switch (op)
    {
        HANDLE_OP(SHL)
        HANDLE_OP(SHR)
        HANDLE_OP(SAL)
        HANDLE_OP(SAR)
    default: return 0;
    }
}

#undef HANDLE_OP

#if defined(__x86_64__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $8, %4\n"                                     \
                     "je 1f\n"                                          \
                     "cmp $4, %4\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %4\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %4\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "1: " #op " %%rax\n"                               \
                     "jmp 5f\n"                                         \
                     "2: " #op " %%eax\n"                               \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%ax\n"                                \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%al\n"                                \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*flags), "=a"(*x), "=r"(ret)                \
                     : "a"(*x), "r"(size), "2"(ret));                   \
    return ret;
#elif defined(__i386__)
#define HANDLE_OP(op)                                                   \
    case X86_INS_ ## op:                                                \
    __asm__ volatile("cmp $4, %4\n"                                     \
                     "je 2f\n"                                          \
                     "cmp $2, %4\n"                                     \
                     "je 3f\n"                                          \
                     "cmp $1, %4\n"                                     \
                     "je 4f\n"                                          \
                     "xor %2, %2\n"                                     \
                     "jmp 6f\n"                                         \
                     "2: " #op " %%eax\n"                               \
                     "jmp 5f\n"                                         \
                     "3: " #op " %%ax\n"                                \
                     "jmp 5f\n"                                         \
                     "4: " #op " %%al\n"                                \
                     "5: pushf\n"                                       \
                     "pop %0\n"                                         \
                     "6:\n"                                             \
                     : "=g"(*(uint32_t*)flags), "=a"(*(uint32_t*)x), "=r"(ret) \
                     : "a"(*(uint32_t*)x), "r"((uint32_t)size), "2"(ret)); \
    return ret;
#endif

static int unary_op_with_flags(uint64_t *x, unsigned int op, uint8_t size, uint64_t *flags)
{
    int ret = 1;
    switch (op)
    {
        HANDLE_OP(NEG)
        HANDLE_OP(NOT)
        HANDLE_OP(INC)
        HANDLE_OP(DEC)
    default: return 0;
    }
}

#undef HANDLE_OP

#define CF_MASK 0x0001
#define PF_MASK 0x0004
#define AF_MASK 0x0010
#define ZF_MASK 0x0040
#define SF_MASK 0x0080
#define OF_MASK 0x0800

static void fix_flags(struct x86emu_ctx *ctx, uint64_t flags, int update_carry)
{
    uint64_t mask = PF_MASK | AF_MASK | ZF_MASK | SF_MASK | OF_MASK;
    uint64_t rflags = safe_get_register_value(ctx, X86_REG_EFLAGS);
    if (update_carry)
        mask |= CF_MASK;
    rflags = (rflags & ~mask) | (flags & mask);
    safe_set_register_value(ctx, X86_REG_EFLAGS, rflags);
}

static int eval_flags_impl(struct x86emu_ctx *ctx, unsigned int op)
{
    uint64_t flags = safe_get_register_value(ctx, X86_REG_EFLAGS);
    switch (op)
    {
    case X86_INS_JE:
    case X86_INS_SETE:
    case X86_INS_CMOVE:
        return flags & ZF_MASK;
    case X86_INS_JNE:
    case X86_INS_SETNE:
    case X86_INS_CMOVNE:
        return !(flags & ZF_MASK);
    case X86_INS_JG:
    case X86_INS_SETG:
    case X86_INS_CMOVG:
        return !(flags & ZF_MASK) && !(flags & SF_MASK) == !(flags & OF_MASK);
    case X86_INS_JGE:
    case X86_INS_SETGE:
    case X86_INS_CMOVGE:
        return !(flags & SF_MASK) == !(flags & OF_MASK);
    case X86_INS_JL:
    case X86_INS_SETL:
    case X86_INS_CMOVL:
        return !(flags & SF_MASK) != !(flags & OF_MASK);
    case X86_INS_JLE:
    case X86_INS_SETLE:
    case X86_INS_CMOVLE:
        return (flags & ZF_MASK) || !(flags & SF_MASK) != !(flags & OF_MASK);
    case X86_INS_JA:
    case X86_INS_SETA:
    case X86_INS_CMOVA:
        return !(flags & CF_MASK) && !(flags & ZF_MASK);
    case X86_INS_JAE:
    case X86_INS_SETAE:
    case X86_INS_CMOVAE:
        return !(flags & CF_MASK);
    case X86_INS_JB:
    case X86_INS_SETB:
    case X86_INS_CMOVB:
        return flags & CF_MASK;
    case X86_INS_JBE:
    case X86_INS_SETBE:
    case X86_INS_CMOVBE:
        return (flags & CF_MASK) || (flags & ZF_MASK);
    case X86_INS_JS:
    case X86_INS_SETS:
    case X86_INS_CMOVS:
        return flags & SF_MASK;
    case X86_INS_JNS:
    case X86_INS_SETNS:
    case X86_INS_CMOVNS:
        return !(flags & SF_MASK);
    case X86_INS_JO:
    case X86_INS_SETO:
    case X86_INS_CMOVO:
        return flags & OF_MASK;
    case X86_INS_JNO:
    case X86_INS_SETNO:
    case X86_INS_CMOVNO:
        return !(flags & OF_MASK);
    case X86_INS_JP:
    case X86_INS_SETP:
    case X86_INS_CMOVP:
        return flags & PF_MASK;
    case X86_INS_JNP:
    case X86_INS_SETNP:
    case X86_INS_CMOVNP:
        return !(flags & PF_MASK);
    default: assert(0);
    }
}

static int eval_flags(struct x86emu_ctx *ctx, unsigned int op)
{
    int ret = eval_flags_impl(ctx, op);
    if (ret)
        TRACE("\1condition satisfied\n");
    else
        TRACE("\1condition NOT satisfied\n");
    return ret;
}

static int sign_extend(uint64_t *x, uint8_t size)
{
    switch (size)
    {
    case 1: *x = (int8_t)*x; return 1;
    case 2: *x = (int16_t)*x; return 1;
    case 4: *x = (int32_t)*x; return 1;
    case 8: *x = (int64_t)*x; return 1;
    default: return 0;
    }
}

static int execute_insn(struct x86emu_ctx *ctx, const cs_insn *insn)
{
    struct cs_x86 *insn_x86 = &insn->detail->x86;
    uint64_t tmp, tmp2, tmp3, flags;
#ifdef __x86_64__
    uint64_t tmp4;
    __uint128_t tmp128, tmp128_2;
    double dtmp, dtmp2;
    float ftmp, ftmp2;
    char vtmp[16];
#endif
    int lock;

    lock = insn_x86->prefix[0] == X86_PREFIX_LOCK;
    if (insn_x86->prefix[0])
    {
        if (lock)
        {
            switch (insn->id)
            {
                case X86_INS_NEG:
                case X86_INS_NOT:
                case X86_INS_INC:
                case X86_INS_DEC:
                case X86_INS_XADD:
                    break;

                default:
                    WARN("unsupported lock prefix\n");
                    return 0;
            }
        }
        else
        {
            WARN("unsupported prefix\n");
            return 0;
        }
    }

    switch (insn->id)
    {
    case X86_INS_LEA:
        if (insn_x86->op_count != 2)
            return 0;
        if (insn_x86->operands[0].type != X86_OP_REG)
            return 0;
        if (insn_x86->operands[1].type != X86_OP_MEM)
            return 0;
        if (!get_memory_addr(ctx, &insn_x86->operands[1].mem, (void**)&tmp, 1))
            return 0;
        if (!set_register_value(ctx, insn_x86->operands[0].reg, tmp))
            return 0;
        return 1;
    case X86_INS_ADD:
    case X86_INS_SUB:
    case X86_INS_ADC:
    case X86_INS_SBB:
    case X86_INS_XOR:
    case X86_INS_OR:
    case X86_INS_AND:
    case X86_INS_CMP:
    case X86_INS_TEST:
        if (insn_x86->op_count != 2)
            return 0;
        if (insn_x86->operands[0].size != insn_x86->operands[1].size)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp2))
            return 0;
        if (!binary_op_with_flags(&tmp, &tmp2, insn->id, insn_x86->operands[0].size, &flags))
            return 0;
        if (insn->id != X86_INS_TEST && insn->id != X86_INS_CMP)
        {
            if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
                return 0;
        }
        fix_flags(ctx, flags, 1);
        return 1;
#ifdef __x86_64__
    case X86_INS_DIV:
        if (insn_x86->op_count != 1)
            return 0;
        if (insn_x86->operands[0].size != 8)
            return 0;
        tmp128 = safe_get_register_value(ctx, X86_REG_RDX);
        tmp128 <<= 64;
        tmp128 |= safe_get_register_value(ctx, X86_REG_RAX);
        tmp128_2 = tmp128;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        tmp128 /= tmp;
        if (tmp128 > UINT64_MAX)
            return 0;
        tmp128_2 %= tmp;
        safe_set_register_value(ctx, X86_REG_RAX, (uint64_t)tmp128);
        safe_set_register_value(ctx, X86_REG_RDX, (uint64_t)tmp128_2);
        return 1;
#endif
    case X86_INS_SHL:
    case X86_INS_SHR:
    case X86_INS_SAL:
    case X86_INS_SAR:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp2))
            return 0;
        if (!shift_with_flags(&tmp, &tmp2, insn->id, insn_x86->operands[0].size, &flags))
            return 0;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
            return 0;
        /* shifts only affects flags when count is not zero */
        if (tmp2 != 0)
            fix_flags(ctx, flags, 1);
        return 1;
    case X86_INS_XADD:
        if (insn_x86->op_count != 2)
            return 0;
        if (insn_x86->operands[0].size != insn_x86->operands[1].size)
            return 0;
        if (insn_x86->operands[1].type != X86_OP_REG)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp2))
            return 0;
        tmp3 = tmp;
        if (!binary_op_with_flags(&tmp, &tmp2, X86_INS_ADD, insn_x86->operands[0].size, &flags))
            return 0;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, lock ? &tmp3 : NULL))
            return 0;
        safe_set_register_value(ctx, insn_x86->operands[1].reg, tmp);
        fix_flags(ctx, flags, 1);
        return 1;
    case X86_INS_NEG:
    case X86_INS_NOT:
    case X86_INS_INC:
    case X86_INS_DEC:
        if (insn_x86->op_count != 1)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        tmp2 = tmp;
        if (!unary_op_with_flags(&tmp, insn->id, insn_x86->operands[0].size, &flags))
            return 0;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, lock ? &tmp2 : NULL))
            return 0;
        if (insn->id != X86_INS_NOT)
            fix_flags(ctx, flags, insn->id != X86_INS_INC && insn->id != X86_INS_DEC);
        return 1;
    case X86_INS_MOV:
    case X86_INS_MOVZX:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp))
            return 0;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
            return 0;
        return 1;
    case X86_INS_MOVSX:
    case X86_INS_MOVSXD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp))
            return 0;
        if (!sign_extend(&tmp, insn_x86->operands[1].size))
            return 0;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
            return 0;
        return 1;
    case X86_INS_CALL:
        if (insn_x86->op_count != 1)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        tmp2 = safe_get_register_value(ctx, ARCH_RSP);
        tmp3 = safe_get_register_value(ctx, ARCH_RIP);
        if (!set_mem((void*)(uintptr_t)(tmp2 - sizeof(void*)), tmp3, sizeof(void*)))
            return 0;
        safe_set_register_value(ctx, ARCH_RSP, tmp2 - sizeof(void*));
        safe_set_register_value(ctx, ARCH_RIP, tmp);
        return 1;
    case X86_INS_RET:
        if (insn_x86->op_count > 1)
            return 0;
        tmp3 = 0;
        if (insn_x86->op_count == 1)
            if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp3))
                return 0;
        tmp2 = safe_get_register_value(ctx, ARCH_RSP);
        if (!get_mem((void*)(uintptr_t)tmp2, &tmp, sizeof(void*)))
            return 0;
        safe_set_register_value(ctx, ARCH_RSP, tmp2 + sizeof(void*) + tmp3);
        safe_set_register_value(ctx, ARCH_RIP, tmp);
        return 1;
    case X86_INS_JMP:
        if (insn_x86->op_count != 1)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        safe_set_register_value(ctx, ARCH_RIP, tmp);
        return 1;
    case X86_INS_JE:
    case X86_INS_JNE:
    case X86_INS_JG:
    case X86_INS_JGE:
    case X86_INS_JL:
    case X86_INS_JLE:
    case X86_INS_JA:
    case X86_INS_JAE:
    case X86_INS_JB:
    case X86_INS_JBE:
    case X86_INS_JS:
    case X86_INS_JNS:
    case X86_INS_JO:
    case X86_INS_JNO:
    case X86_INS_JP:
    case X86_INS_JNP:
        if (insn_x86->op_count != 1)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        if (eval_flags(ctx, insn->id))
            safe_set_register_value(ctx, ARCH_RIP, tmp);
        return 1;
    case X86_INS_SETE:
    case X86_INS_SETNE:
    case X86_INS_SETG:
    case X86_INS_SETGE:
    case X86_INS_SETL:
    case X86_INS_SETLE:
    case X86_INS_SETA:
    case X86_INS_SETAE:
    case X86_INS_SETB:
    case X86_INS_SETBE:
    case X86_INS_SETS:
    case X86_INS_SETNS:
    case X86_INS_SETO:
    case X86_INS_SETNO:
    case X86_INS_SETP:
    case X86_INS_SETNP:
        if (insn_x86->op_count != 1)
            return 0;
        tmp = eval_flags(ctx, insn->id);
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
            return 0;
        return 1;
    case X86_INS_CMOVE:
    case X86_INS_CMOVNE:
    case X86_INS_CMOVG:
    case X86_INS_CMOVGE:
    case X86_INS_CMOVL:
    case X86_INS_CMOVLE:
    case X86_INS_CMOVA:
    case X86_INS_CMOVAE:
    case X86_INS_CMOVB:
    case X86_INS_CMOVBE:
    case X86_INS_CMOVS:
    case X86_INS_CMOVNS:
    case X86_INS_CMOVO:
    case X86_INS_CMOVNO:
    case X86_INS_CMOVP:
    case X86_INS_CMOVNP:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp))
            return 0;
        if (eval_flags(ctx, insn->id))
            if (!set_operand_value(ctx, &insn_x86->operands[0], tmp, NULL))
                return 0;
        return 1;
    case X86_INS_PUSH:
        if (insn_x86->op_count != 1)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp))
            return 0;
        tmp2 = safe_get_register_value(ctx, ARCH_RSP);
        tmp2 -= insn_x86->prefix[2] ? 2 : insn_x86->operands[0].size;
        if (!set_mem((void*)(uintptr_t)tmp2, tmp, insn_x86->prefix[2] ? 2 : insn_x86->operands[0].size))
            return 0;
        safe_set_register_value(ctx, ARCH_RSP, tmp2);
        return 1;
    case X86_INS_POP:
        if (insn_x86->op_count != 1)
            return 0;
        tmp = safe_get_register_value(ctx, ARCH_RSP);
        if (!get_mem((void*)(uintptr_t)tmp, &tmp2, insn_x86->operands[0].size))
            return 0;
        tmp += insn_x86->operands[0].size;
        if (!set_operand_value(ctx, &insn_x86->operands[0], tmp2, NULL))
            return 0;
        safe_set_register_value(ctx, ARCH_RSP, tmp);
        return 1;
    case X86_INS_POPFQ:
        if (insn_x86->op_count != 0)
            return 0;
        tmp = safe_get_register_value(ctx, ARCH_RSP);
        if (!get_mem((void*)(uintptr_t)tmp, &tmp2, sizeof(void*)))
            return 0;
        tmp += sizeof(void*);
        safe_set_register_value(ctx, ARCH_RSP, tmp);
        safe_set_register_value(ctx, X86_REG_EFLAGS, tmp2);
        return 1;
    case X86_INS_BT:
        if (insn_x86->op_count != 2)
            return 0;
        if (insn_x86->operands[0].type == X86_OP_REG)
        {
            if (!get_register_value(ctx, insn_x86->operands[0].reg, &tmp))
                return 0;
            if (!get_operand_value(ctx, &insn_x86->operands[0], &tmp2))
                return 0;
            tmp2 %= insn_x86->operands[0].size * 8;
            tmp = !!(tmp & (1 << tmp2));
            tmp3 = safe_get_register_value(ctx, X86_REG_EFLAGS);
            tmp3 = (tmp3 & ~CF_MASK) | (tmp * CF_MASK);
            safe_set_register_value(ctx, X86_REG_EFLAGS, tmp3);
        }
        else
        {
            return 0;
        }
        return 1;
#ifdef __x86_64__
    case X86_INS_MOVAPS:
    case X86_INS_MOVUPS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, vtmp, 16);
        memcpy((void*)tmp, vtmp, 16);
        return 1;
    case X86_INS_MOVSS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, vtmp, 4);
        if (is_operand_xmm_register(&insn_x86->operands[0]) && is_operand_memory(&insn_x86->operands[1]))
            memset((void*)tmp, 0, 16);
        memcpy((void*)tmp, vtmp, 4);
        return 1;
    case X86_INS_MOVSD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, vtmp, 8);
        if (is_operand_xmm_register(&insn_x86->operands[0]) && is_operand_memory(&insn_x86->operands[1]))
            memset((void*)tmp, 0, 16);
        memcpy((void*)tmp, vtmp, 8);
        return 1;
    case X86_INS_CVTSD2SS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, &dtmp, sizeof(dtmp));
        *(float*)tmp = dtmp;
        return 1;
    case X86_INS_CVTSS2SD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, &ftmp, sizeof(ftmp));
        *(double*)tmp = ftmp;
        return 1;
    case X86_INS_CVTPD2PS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, &dtmp, sizeof(dtmp));
        read_helper((double*)tmp2 + 1, &dtmp2, sizeof(dtmp2));
        memset((void*)tmp, 0, 16);
        *(float*)tmp = dtmp;
        *((float*)tmp + 1) = dtmp2;
        return 1;
    case X86_INS_CVTPS2PD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp2, &ftmp, sizeof(ftmp));
        read_helper((float*)tmp2 + 1, &ftmp2, sizeof(ftmp2));
        *(double*)tmp = ftmp;
        *((double*)tmp + 1) = ftmp2;
        return 1;
    case X86_INS_UCOMISS:
    case X86_INS_COMISS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &ftmp, sizeof(ftmp));
        read_helper((void*)tmp2, &ftmp2, sizeof(ftmp2));
        if (ftmp < ftmp2)
            fix_flags(ctx, CF_MASK, 1);
        else if (ftmp == ftmp2)
            fix_flags(ctx, ZF_MASK, 1);
        else if (ftmp > ftmp2)
            fix_flags(ctx, 0, 1);
        else
            fix_flags(ctx, ZF_MASK | PF_MASK | CF_MASK, 1);
        return 1;
    case X86_INS_UCOMISD:
    case X86_INS_COMISD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        read_helper((void*)tmp2, &dtmp2, sizeof(dtmp2));
        if (dtmp < dtmp2)
            fix_flags(ctx, CF_MASK, 1);
        else if (dtmp == dtmp2)
            fix_flags(ctx, ZF_MASK, 1);
        else if (dtmp > dtmp2)
            fix_flags(ctx, 0, 1);
        else
            fix_flags(ctx, ZF_MASK | PF_MASK | CF_MASK, 1);
        return 1;
    case X86_INS_ADDSD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        read_helper((void*)tmp2, &dtmp2, sizeof(dtmp2));
        *(double*)tmp = dtmp + dtmp2;
        return 1;
    case X86_INS_SUBSD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        read_helper((void*)tmp2, &dtmp2, sizeof(dtmp2));
        *(double*)tmp = dtmp - dtmp2;
        return 1;
    case X86_INS_MULSD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        read_helper((void*)tmp2, &dtmp2, sizeof(dtmp2));
        *(double*)tmp = dtmp * dtmp2;
        return 1;
    case X86_INS_DIVSD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        read_helper((void*)tmp2, &dtmp2, sizeof(dtmp2));
        *(double*)tmp = dtmp / dtmp2;
        return 1;
    case X86_INS_ANDPS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &tmp3, sizeof(tmp3));
        read_helper((void*)tmp2, &tmp4, sizeof(tmp4));
        *(uint64_t*)tmp = tmp3 & tmp4;
        read_helper((uint64_t*)tmp + 1, &tmp3, sizeof(tmp3));
        read_helper((uint64_t*)tmp2 + 1, &tmp4, sizeof(tmp4));
        *((uint64_t*)tmp + 1) = tmp3 & tmp4;
        return 1;
    case X86_INS_XORPS:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp, 1))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp2, 1))
            return 0;
        read_helper((void*)tmp, &tmp3, sizeof(tmp3));
        read_helper((void*)tmp2, &tmp4, sizeof(tmp4));
        *(uint64_t*)tmp = tmp3 ^ tmp4;
        read_helper((uint64_t*)tmp + 1, &tmp3, sizeof(tmp3));
        read_helper((uint64_t*)tmp2 + 1, &tmp4, sizeof(tmp4));
        *((uint64_t*)tmp + 1) = tmp3 ^ tmp4;
        return 1;
    case X86_INS_CVTTSD2SI:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[1], (void**)&tmp, 1))
            return 0;
        read_helper((void*)tmp, &dtmp, sizeof(dtmp));
        switch (insn_x86->operands[0].size)
        {
            case 4:
                if (dtmp > INT32_MAX || dtmp < INT32_MIN)
                    tmp2 = 0x8000000;
                else
                    tmp2 = (int32_t)dtmp;
            case 8:
                if (dtmp > INT64_MAX || dtmp < INT64_MIN)
                    tmp2 = 0x8000000000000000;
                else
                    tmp2 = (int64_t)dtmp;
                break;
            default: return 0;
        }
        set_operand_value(ctx, &insn_x86->operands[0], tmp2, NULL);
        return 1;
    case X86_INS_CVTSI2SD:
        if (insn_x86->op_count != 2)
            return 0;
        if (!get_operand_value(ctx, &insn_x86->operands[1], &tmp))
            return 0;
        if (!get_operand_addr(ctx, &insn_x86->operands[0], (void**)&tmp2, 1))
            return 0;
        switch (insn_x86->operands[1].size)
        {
            case 4:
                dtmp = (int32_t)tmp;
                break;
            case 8:
                dtmp = (int64_t)tmp;
                break;
            default:
                return 0;
        }
        *(double*)tmp2 = dtmp;
        return 1;
#endif
    case X86_INS_NOP:
        return 1;
    default:
        return 0;
    }
}

int x86emu_init_ctx(struct x86emu_ctx *ctx)
{
    memset(ctx, 0, sizeof(*ctx));

    if (cs_open(CS_ARCH_X86, sizeof(void*) == 8 ? CS_MODE_64 : CS_MODE_32, &ctx->handle) != CS_ERR_OK)
    {
        ERR("Cannot initialize capstone\n");
        return 0;
    }

    if (cs_option(ctx->handle, CS_OPT_DETAIL, CS_OPT_ON) != CS_ERR_OK)
    {
        ERR("cannot set capstone option\n");
        cs_close(&ctx->handle);
        return 0;
    }

    if (!(ctx->insn = cs_malloc(ctx->handle)))
    {
        cs_close(&ctx->handle);
        ERR("cannot allocate capstone buffer\n");
        return 0;
    }

    return 1;
}

int x86emu_destroy_ctx(struct x86emu_ctx *ctx)
{
    cs_free(ctx->insn, 1);
    cs_close(&ctx->handle);
    return 1;
}

void x86emu_dump_state(struct x86emu_ctx *ctx)
{
    uint64_t rflags = safe_get_register_value(ctx, X86_REG_EFLAGS);
    TRACE("\1RIP %016"PRIx64" RFLAGS %016"PRIx64" (%c %c %c %c %c %c)\n",
            safe_get_register_value(ctx, ARCH_RIP), rflags,
            rflags & CF_MASK ? 'C' : 'c',
            rflags & PF_MASK ? 'P' : 'p',
            rflags & AF_MASK ? 'A' : 'a',
            rflags & ZF_MASK ? 'Z' : 'z',
            rflags & SF_MASK ? 'S' : 's',
            rflags & OF_MASK ? 'O' : 'o');
    TRACE("\1RAX %016"PRIx64" RBX %016"PRIx64" RCX %016"PRIx64" RDX %016"PRIx64" "
            "RSP %016"PRIx64" RBP %016"PRIx64" RSI %016"PRIx64" RDI %016"PRIx64"\n",
            safe_get_register_value(ctx, X86_REG_RAX), safe_get_register_value(ctx, X86_REG_RBX),
            safe_get_register_value(ctx, X86_REG_RCX), safe_get_register_value(ctx, X86_REG_RDX),
            safe_get_register_value(ctx, X86_REG_RSP), safe_get_register_value(ctx, X86_REG_RBP),
            safe_get_register_value(ctx, X86_REG_RSI), safe_get_register_value(ctx, X86_REG_RDI));
    TRACE("\1 R8 %016"PRIx64"  R9 %016"PRIx64" R10 %016"PRIx64" R11 %016"PRIx64" "
            "R12 %016"PRIx64" R13 %016"PRIx64" R14 %016"PRIx64" R15 %016"PRIx64"\n",
            safe_get_register_value(ctx, X86_REG_R8), safe_get_register_value(ctx, X86_REG_R9),
            safe_get_register_value(ctx, X86_REG_R10), safe_get_register_value(ctx, X86_REG_R11),
            safe_get_register_value(ctx, X86_REG_R12), safe_get_register_value(ctx, X86_REG_R13),
            safe_get_register_value(ctx, X86_REG_R14), safe_get_register_value(ctx, X86_REG_R15));
    TRACE("\1 XMM0 %016"PRIx64"%016"PRIx64"  XMM1 %016"PRIx64"%016"PRIx64"  XMM2 %016"PRIx64"%016"PRIx64"  XMM3 %016"PRIx64"%016"PRIx64"\n",
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM0) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM0),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM1) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM1),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM2) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM2),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM3) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM3));
    TRACE("\1 XMM4 %016"PRIx64"%016"PRIx64"  XMM5 %016"PRIx64"%016"PRIx64"  XMM6 %016"PRIx64"%016"PRIx64"  XMM7 %016"PRIx64"%016"PRIx64"\n",
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM4) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM4),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM5) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM5),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM6) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM6),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM7) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM7));
    TRACE("\1 XMM8 %016"PRIx64"%016"PRIx64"  XMM9 %016"PRIx64"%016"PRIx64" XMM10 %016"PRIx64"%016"PRIx64" XMM11 %016"PRIx64"%016"PRIx64"\n",
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM8) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM8),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM9) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM9),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM10) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM10),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM11) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM11));
    TRACE("\1XMM12 %016"PRIx64"%016"PRIx64" XMM13 %016"PRIx64"%016"PRIx64" XMM14 %016"PRIx64"%016"PRIx64" XMM15 %016"PRIx64"%016"PRIx64"\n",
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM12) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM12),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM13) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM13),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM14) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM14),
            *((uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM15) + 1), *(uint64_t*)safe_get_register_addr(ctx, X86_REG_XMM15));
}

int x86emu_execute_step(struct x86emu_ctx *ctx)
{
    uintptr_t prev_rip = safe_get_register_value(ctx, ARCH_RIP);
    const uint8_t *code = ctx->override_code ? (const uint8_t*)ctx->override_code : (const uint8_t*)prev_rip;
    size_t size = 16;
    uint64_t address = prev_rip;

    if (!cs_disasm_iter(ctx->handle, &code, &size, &address, ctx->insn))
    {
        TRACE("step failed: cannot decode\n");
        return 0;
    }

    __wine_dump_symbol((void*)(uintptr_t)ctx->insn->address);
    TRACE("\1executing %"PRIx64": %s %s\n", ctx->insn->address, ctx->insn->mnemonic, ctx->insn->op_str);

    if (ctx->override_code)
        safe_set_register_value(ctx, ARCH_RIP, (uintptr_t)code - (uintptr_t)ctx->override_code + prev_rip);
    else
        safe_set_register_value(ctx, ARCH_RIP, (uintptr_t)code);

    if (!execute_insn(ctx, ctx->insn))
    {
        TRACE("step failed: cannot execute\n");
        safe_set_register_value(ctx, ARCH_RIP, prev_rip);
        return 0;
    }

    return 1;
}

#else

int x86emu_init_ctx(struct x86emu_ctx *ctx)
{
    ERR("x86emu not supported on 32 bit\n");
    return 0;
}

int x86emu_destroy_ctx(struct x86emu_ctx *ctx)
{
    ERR("x86emu not supported on 32 bit\n");
    return 0;
}

int x86emu_execute_step(struct x86emu_ctx *ctx)
{
    ERR("x86emu not supported on 32 bit\n");
    return 0;
}

void x86emu_dump_state(struct x86emu_ctx *ctx)
{
    ERR("x86emu not supported on 32 bit\n");
}

#endif

void x86emu_run_emulator(ucontext_t *ucontext)
{
    struct x86emu_ctx ctx;
    unsigned int i, step_num = 128;
    char *step_num_str = getenv("WINE_X86EMU_STEP_NUM");

    if (step_num_str)
        step_num = atoi(step_num_str);
    TRACE("executing %u steps\n", step_num);

    x86emu_init_ctx(&ctx);
    ctx.ctx = ucontext;
    for (i = 0; i < step_num; i++)
    {
        x86emu_dump_state(&ctx);
        TRACE("executing step %u\n", i);
        if (!x86emu_execute_step(&ctx))
            break;
    }
    x86emu_dump_state(&ctx);
    x86emu_destroy_ctx(&ctx);
}

/*
 * Debugging functions
 *
 * Copyright 2000 Alexandre Julliard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
#pragma makedep unix
#endif

#include "config.h"

#include <assert.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>
#include <execinfo.h>

#include <capstone/capstone.h>

#ifdef HAVE_LIBUNWIND
# define UNW_LOCAL_ONLY
# include <libunwind.h>
#endif

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winnt.h"
#include "winternl.h"
#include "unix_private.h"
#include "wine/debug.h"

WINE_DECLARE_DEBUG_CHANNEL(pid);
WINE_DECLARE_DEBUG_CHANNEL(timestamp);
WINE_DEFAULT_DEBUG_CHANNEL(ntdll);
WINE_DECLARE_DEBUG_CHANNEL(microsecs);
WINE_DECLARE_DEBUG_CHANNEL(disas);
WINE_DECLARE_DEBUG_CHANNEL(backtrace);
WINE_DECLARE_DEBUG_CHANNEL(signal);

struct debug_info
{
    unsigned int str_pos;       /* current position in strings buffer */
    unsigned int out_pos;       /* current position in output buffer */
    char         strings[1020]; /* buffer for temporary strings */
    char         output[1020];  /* current output line */
};

C_ASSERT( sizeof(struct debug_info) == 0x800 );

static BOOL init_done;
static struct debug_info initial_info;  /* debug info for initial thread */
static unsigned char default_flags = (1 << __WINE_DBCL_ERR) | (1 << __WINE_DBCL_FIXME);
static int nb_debug_options = -1;
static int options_size;
static struct __wine_debug_channel *debug_options;

static const char * const debug_classes[] = { "fixme", "err", "warn", "trace" };

/* get the debug info pointer for the current thread */
static inline struct debug_info *get_info(void)
{
    if (!init_done) return &initial_info;
#ifdef _WIN64
    return (struct debug_info *)((TEB32 *)((char *)NtCurrentTeb() + teb_offset) + 1);
#else
    return (struct debug_info *)(NtCurrentTeb() + 1);
#endif
}

/* add a string to the output buffer */
static int append_output( struct debug_info *info, const char *str, size_t len )
{
    if (len >= sizeof(info->output) - info->out_pos)
    {
       fprintf( stderr, "wine_dbg_output: debugstr buffer overflow (contents: '%s')\n", info->output );
       info->out_pos = 0;
       abort();
    }
    memcpy( info->output + info->out_pos, str, len );
    info->out_pos += len;
    return len;
}

/* add a new debug option at the end of the option list */
static void add_option( const char *name, unsigned char set, unsigned char clear )
{
    int min = 0, max = nb_debug_options - 1, pos, res;

    if (!name[0])  /* "all" option */
    {
        default_flags = (default_flags & ~clear) | set;
        return;
    }
    if (strlen(name) >= sizeof(debug_options[0].name)) return;

    while (min <= max)
    {
        pos = (min + max) / 2;
        res = strcmp( name, debug_options[pos].name );
        if (!res)
        {
            debug_options[pos].flags = (debug_options[pos].flags & ~clear) | set;
            return;
        }
        if (res < 0) max = pos - 1;
        else min = pos + 1;
    }
    if (nb_debug_options >= options_size)
    {
        options_size = max( options_size * 2, 16 );
        debug_options = realloc( debug_options, options_size * sizeof(debug_options[0]) );
    }

    pos = min;
    if (pos < nb_debug_options) memmove( &debug_options[pos + 1], &debug_options[pos],
                                         (nb_debug_options - pos) * sizeof(debug_options[0]) );
    strcpy( debug_options[pos].name, name );
    debug_options[pos].flags = (default_flags & ~clear) | set;
    nb_debug_options++;
}

/* parse a set of debugging option specifications and add them to the option list */
static void parse_options( const char *str, const char *app_name )
{
    char *opt, *next, *options;
    unsigned int i;

    if (!(options = strdup(str))) return;
    for (opt = options; opt; opt = next)
    {
        char *p;
        unsigned char set = 0, clear = 0;

        if ((next = strchr( opt, ',' ))) *next++ = 0;

        if ((p = strchr( opt, ':' )))
        {
            *p = 0;
            if (strcasecmp( opt, app_name )) continue;
            opt = p + 1;
        }

        p = opt + strcspn( opt, "+-" );
        if (!p[0]) p = opt;  /* assume it's a debug channel name */

        if (p > opt)
        {
            for (i = 0; i < ARRAY_SIZE(debug_classes); i++)
            {
                int len = strlen(debug_classes[i]);
                if (len != (p - opt)) continue;
                if (!memcmp( opt, debug_classes[i], len ))  /* found it */
                {
                    if (*p == '+') set |= 1 << i;
                    else clear |= 1 << i;
                    break;
                }
            }
            if (i == ARRAY_SIZE(debug_classes)) /* bad class name, skip it */
                continue;
        }
        else
        {
            if (*p == '-') clear = ~0;
            else set = ~0;
        }
        if (*p == '+' || *p == '-') p++;
        if (!p[0]) continue;

        if (!strcmp( p, "all" ))
            default_flags = (default_flags & ~clear) | set;
        else
            add_option( p, set, clear );
    }
    free( options );
}

/* print the usage message */
static void debug_usage(void)
{
    static const char usage[] =
        "Syntax of the WINEDEBUG variable:\n"
        "  WINEDEBUG=[[process:]class]+xxx,[[process:]class]-yyy,...\n\n"
        "Example: WINEDEBUG=+relay,warn-heap\n"
        "    turns on relay traces, disable heap warnings\n"
        "Available message classes: err, warn, fixme, trace\n";
    write( 2, usage, sizeof(usage) - 1 );
    exit(1);
}

/* initialize all options at startup */
static void init_options(void)
{
    char *wine_debug = getenv("WINEDEBUG");
    const char *app_name, *p;
    struct stat st1, st2;

    nb_debug_options = 0;

    /* check for stderr pointing to /dev/null */
    if (!fstat( 2, &st1 ) && S_ISCHR(st1.st_mode) &&
        !stat( "/dev/null", &st2 ) && S_ISCHR(st2.st_mode) &&
        st1.st_rdev == st2.st_rdev)
    {
        default_flags = 0;
        return;
    }
    if (!wine_debug) return;
    if (!strcmp( wine_debug, "help" )) debug_usage();

    app_name = main_argv[1];
    while ((p = strpbrk( app_name, "/\\" ))) app_name = p + 1;

    parse_options( wine_debug, app_name );
}

/***********************************************************************
 *		__wine_dbg_get_channel_flags  (NTDLL.@)
 *
 * Get the flags to use for a given channel, possibly setting them too in case of lazy init
 */
unsigned char __cdecl __wine_dbg_get_channel_flags( struct __wine_debug_channel *channel )
{
    int min, max, pos, res;
    unsigned char flags;

    if (!(channel->flags & (1 << __WINE_DBCL_INIT))) return channel->flags;

    if (nb_debug_options == -1) init_options();

    flags = default_flags;
    min = 0;
    max = nb_debug_options - 1;
    while (min <= max)
    {
        pos = (min + max) / 2;
        res = strcmp( channel->name, debug_options[pos].name );
        if (!res)
        {
            flags = debug_options[pos].flags;
            break;
        }
        if (res < 0) max = pos - 1;
        else min = pos + 1;
    }

    if (!(flags & (1 << __WINE_DBCL_INIT))) channel->flags = flags; /* not dynamically changeable */
    return flags;
}

/***********************************************************************
 *		__wine_dbg_strdup  (NTDLL.@)
 */
const char * __cdecl __wine_dbg_strdup( const char *str )
{
    struct debug_info *info = get_info();
    unsigned int pos = info->str_pos;
    size_t n = strlen( str ) + 1;

    assert( n <= sizeof(info->strings) );
    if (pos + n > sizeof(info->strings)) pos = 0;
    info->str_pos = pos + n;
    return memcpy( info->strings + pos, str, n );
}

/***********************************************************************
 *		unixcall_wine_dbg_write
 */
NTSTATUS unixcall_wine_dbg_write( void *args )
{
    struct wine_dbg_write_params *params = args;

    return write( 2, params->str, params->len );
}

#ifdef _WIN64
/***********************************************************************
 *		wow64_wine_dbg_write
 */
NTSTATUS wow64_wine_dbg_write( void *args )
{
    struct
    {
        ULONG        str;
        unsigned int len;
    } const *params32 = args;

    return write( 2, ULongToPtr(params32->str), params32->len );
}
#endif

/***********************************************************************
 *		__wine_dbg_output  (NTDLL.@)
 */
int __cdecl __wine_dbg_output( const char *str )
{
    struct debug_info *info = get_info();
    const char *end = strrchr( str, '\n' );
    int ret = 0;

    if (end)
    {
        ret += append_output( info, str, end + 1 - str );
        write( 2, info->output, info->out_pos );
        info->out_pos = 0;
        str = end + 1;
    }
    if (*str) ret += append_output( info, str, strlen( str ));
    return ret;
}

/***********************************************************************
 *		__wine_dbg_header  (NTDLL.@)
 */
int __cdecl __wine_dbg_header( enum __wine_debug_class cls, struct __wine_debug_channel *channel,
                               const char *function, const char *file, int line, void *retaddr,
                               int suppress )
{
    static const char * const wine_prefix = "/wine/";
    static const char * const classes[] = { "fixme", "err", "warn", "trace" };
    struct debug_info *info = get_info();
    char *pos = info->output;
    const char *file_suffix;

    if (!(__wine_dbg_get_channel_flags( channel ) & (1 << cls))) return -1;

    /* only print header if we are at the beginning of the line */
    if (info->out_pos) return 0;

    if (init_done)
    {
        if (TRACE_ON(timestamp))
        {
            UINT ticks = NtGetTickCount();
            pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%3u.%03u:", ticks / 1000, ticks % 1000 );
        }
        if (TRACE_ON(microsecs))
        {
            LARGE_INTEGER counter, frequency, microsecs;
            NtQueryPerformanceCounter(&counter, &frequency);
            microsecs.QuadPart = counter.QuadPart / (frequency.QuadPart / 1000000);
            pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%u.%06u:", (unsigned int)(microsecs.QuadPart / 1000000), (unsigned int)(microsecs.QuadPart % 1000000) );
        }
        if (TRACE_ON(pid)) pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%04x:", (UINT)GetCurrentProcessId() );
        pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%04x:", (UINT)GetCurrentThreadId() );
    }
    if (cls < ARRAY_SIZE( classes ))
    {
        file_suffix = strstr(file, wine_prefix);
        if (file_suffix)
            file_suffix += sizeof(wine_prefix) - 2;
        else
            file_suffix = file;
        pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%s:%s%s:%uU:%p:%s:%d:%s ",
                         classes[cls], suppress ? "@" : "", channel->name, (unsigned)sizeof(void*), retaddr, file_suffix, line, function );
    }
    info->out_pos = pos - info->output;
    return info->out_pos;
}

/***********************************************************************
 *		dbg_init
 */
void dbg_init(void)
{
    struct __wine_debug_channel *options, default_option = { default_flags };

    setbuf( stdout, NULL );
    setbuf( stderr, NULL );

    if (nb_debug_options == -1) init_options();

    options = (struct __wine_debug_channel *)((char *)peb + (is_win64 ? 2 : 1) * page_size);
    memcpy( options, debug_options, nb_debug_options * sizeof(*options) );
    free( debug_options );
    debug_options = options;
    options[nb_debug_options] = default_option;
    init_done = TRUE;
}


/***********************************************************************
 *              NtTraceControl  (NTDLL.@)
 */
NTSTATUS WINAPI NtTraceControl( ULONG code, void *inbuf, ULONG inbuf_len,
                                void *outbuf, ULONG outbuf_len, ULONG *size )
{
    FIXME( "code %u, inbuf %p, inbuf_len %u, outbuf %p, outbuf_len %u, size %p\n",
           (int)code, inbuf, (int)inbuf_len, outbuf, (int)outbuf_len, size );
    return STATUS_SUCCESS;
}


/***********************************************************************
 *              NtSetDebugFilterState  (NTDLL.@)
 */
NTSTATUS WINAPI NtSetDebugFilterState( ULONG component_id, ULONG level, BOOLEAN state )
{
    FIXME( "component_id %#x, level %u, state %#x stub.\n", (int)component_id, (int)level, state );

    return STATUS_SUCCESS;
}

static NTSTATUS unix_echo( void *args )
{
    struct gio_unix_echo_data *data = args;

    GTRACE("echo %d\n", data->request);

    data->reply = data->request;

    return STATUS_SUCCESS;
}

static NTSTATUS disassemble_memory( void *args )
{
    struct gio_unix_disassemble_memory *data = args;
    size_t i, j, actual_count;
    cs_insn *insn;
    cs_mode mode;
    cs_arch arch;
    csh cs;

#if defined(__x86_64__)
    arch = CS_ARCH_X86;
    mode = CS_MODE_64;
#elif defined(__i386__)
    arch = CS_ARCH_X86;
    mode = CS_MODE_32;
#else
    ERR_(disas)("unsupported architecture for disassembling code\n");
    return;
#endif

    if (cs_open(arch, mode, &cs) != CS_ERR_OK)
    {
        ERR_(disas)("cannot initialize capstone\n");
        return STATUS_DEVICE_BUSY;
    }

    actual_count = cs_disasm(cs, (const uint8_t*)(uintptr_t)data->address, data->size,
            data->address, data->insn_count, &insn);

    for (i = 0; i < actual_count; ++i)
    {
        TRACE_(disas)("\0010x%"PRIx64":", insn[i].address);
        for (j = 0; j < insn[i].size; ++j)
        {
            TRACE_(disas)(" %02x", (unsigned int)((const uint8_t*)(uintptr_t)insn[i].address)[j]);
        }
        for ( ; j < 16; ++j)
        {
            TRACE_(disas)("   ");
        }
        TRACE_(disas)(" %s %s\n", insn[i].mnemonic, insn[i].op_str);
    }

    cs_free(insn, actual_count);
    cs_close(&cs);

    return STATUS_SUCCESS;
}

static NTSTATUS dump_symbol(void *args)
{
    struct gio_unix_dump_symbol_params *data = args;
    struct maps_line *maps = scan_maps(), *line;

    line = find_map(maps, data->addr);

    TRACE_(backtrace)("\1backtrace_frame fp=0x%lx, ip=0x%lx, off=0x%lx, file=%s\n",
            0ul, (unsigned long)data->addr,
            line ? (unsigned long)data->addr - (unsigned long)line->from + (unsigned long)line->off : 0,
            line ? line->file : "<unknown>");

    free_maps_lines(maps);

    return STATUS_SUCCESS;
}

struct rewind_context
{
    CONTEXT *context;
    uintptr_t ip;
    uintptr_t fp;
    BOOL finished;
    BOOL first_done;
};

extern void *__wine_unix_call_dispatcher_root;
extern void *__wine_syscall_dispatcher_root;

static void unix_backtrace(struct maps_line *maps)
{
#ifdef HAVE_LIBUNWIND
    struct maps_line *line;
    unw_context_t context;
    unw_cursor_t cursor;
    unw_word_t ip, fp;

    TRACE_(backtrace)("start of UNIX backtrace\n");

    unw_getcontext(&context);
    unw_init_local(&cursor, &context);
    do
    {
        unw_get_reg(&cursor, UNW_REG_IP, &ip);
#if defined(__x86_64__)
        unw_get_reg(&cursor, UNW_X86_64_RBP, &fp);
#elif defined(__i386__)
        unw_get_reg(&cursor, UNW_X86_EBP, &fp);
#else
#error "Unknown architecture"
#endif
        line = find_map(maps, (void *)ip);
        TRACE_(backtrace)("\1backtrace_frame fp=0x%"PRIxPTR", ip=0x%"PRIxPTR", off=0x%"PRIxPTR", file=%s\n",
                fp, ip, line ? ip - (uintptr_t)line->from + (uintptr_t)line->off : 0,
                line ? line->file : "<unknown>");
    }
    while ((void*)ip != (void*)&__wine_unix_call_dispatcher_root
            && (void*)ip != (void*)&__wine_syscall_dispatcher_root
            && unw_step(&cursor) > 0);

    TRACE_(backtrace)("end of UNIX backtrace\n");

#else
    struct maps_line *line;
    void *buf[128];
    int i, count;

    count = backtrace(buf, ARRAY_SIZE(buf));

    TRACE_(backtrace)("start of UNIX backtrace\n");

    for (i = 0; i < count; ++i)
    {
        const uintptr_t ip = (uintptr_t)buf[i];
        const uintptr_t fp = 0;
        line = find_map(maps, (void *)ip);
        TRACE_(backtrace)("\1backtrace_frame fp=0x%"PRIxPTR", ip=0x%"PRIxPTR", off=0x%"PRIxPTR", file=%s\n",
                fp, ip, line ? ip - (uintptr_t)line->from + (uintptr_t)line->off : 0,
                line ? line->file : "<unknown>");

        if ((void*)ip == (void*)&__wine_unix_call_dispatcher_root
                || (void*)ip == (void*)&__wine_syscall_dispatcher_root)
            break;
    }

    TRACE_(backtrace)("end of UNIX backtrace\n");
#endif
}

static void win_backtrace_from_context(CONTEXT *context, struct maps_line *maps)
{
    struct rewind_context ctx = { context }, *ctx_ptr = &ctx;
    void *ret_ptr;
    ULONG ret_len;

    if (!NtCurrentTeb()->Peb->KernelCallbackTable)
    {
        TRACE_(backtrace)("Cannot generate Win backtrace, user32 not initialized yet\n");
        return;
    }

    TRACE_(backtrace)("start of Win backtrace\n");

    for (;;)
    {
        struct maps_line *line;
        NTSTATUS status;

        if ((status = KeUserModeCallback(NtUserCallRewindFrame, &ctx_ptr, sizeof(ctx_ptr), &ret_ptr, &ret_len)) != STATUS_SUCCESS)
        {
            TRACE_(backtrace)("cannot rewind backtrace, status %x\n", (int)status);
            break;
        }

        if (ctx.finished)
            break;

        line = find_map(maps, (void *)ctx.ip);
        TRACE_(backtrace)("\1backtrace_frame fp=0x%"PRIxPTR", ip=0x%"PRIxPTR", off=0x%"PRIxPTR", file=%s\n",
                ctx.fp, ctx.ip, line ? ctx.ip - (uintptr_t)line->from + (uintptr_t)line->off : 0,
                line ? line->file : "<unknown>");
    }

    TRACE_(backtrace)("end of Win backtrace\n");
}

static NTSTATUS dump_backtrace_from_context(void *args)
{
    struct gio_unix_backtrace_from_context *data = args;
    struct maps_line *maps = scan_maps();

    win_backtrace_from_context(data->context, maps);

    free_maps_lines(maps);

    return STATUS_SUCCESS;
}

static NTSTATUS dump_backtrace(void *args)
{
    struct maps_line *maps = scan_maps();
    CONTEXT context = {0};

    unix_backtrace(maps);

    context_from_syscall_frame(&context);
    win_backtrace_from_context(&context, maps);

    free_maps_lines(maps);

    return STATUS_SUCCESS;
}

static NTSTATUS write_to_file( void *args )
{
    struct gio_unix_write_to_file *data = args;
    FILE *file;

    if (data->size == 0)
        return STATUS_INVALID_BUFFER_SIZE;

    file = fopen(data->filename, data->truncate ? "wb" : "r+b");
    if (!file)
    {
        GERR("cannot open file %s\n", data->filename);
        return STATUS_OPEN_FAILED;
    }

    if (fseek(file, data->offset, SEEK_SET) == -1)
    {
        GERR("cannot seek in file %s\n", data->filename);
    }
    else
    {
        if (fwrite(data->data, 1, data->size, file) != data->size)
            GERR("cannot write to file %s\n", data->filename);
    }

    if (fclose(file) != 0)
        GERR("cannot close file %s\n", data->filename);

    return STATUS_SUCCESS;
}

static CONTEXT cached_context = {0};

NTSTATUS set_watchpoints_for_thread( DWORD thread_id )
{
    OBJECT_ATTRIBUTES attr = {0};
    HANDLE handle = NULL;
    CLIENT_ID cid = {0};
    NTSTATUS status;

    attr.Length = sizeof(attr);
    cid.UniqueThread = ULongToHandle(thread_id);
    if ((status = NtOpenThread(&handle, THREAD_ALL_ACCESS, &attr, &cid)))
    {
        GERR("failed\n");
        goto error;
    }

    if ((status = NtSetContextThread(handle, &cached_context)))
    {
        GERR("failed\n");
        goto error;
    }

    status = NtClose(handle);
    handle = NULL;

error:
    if (handle)
        NtClose(handle);
    return status;
}

static NTSTATUS retrieve_system_process_information(SYSTEM_PROCESS_INFORMATION **out)
{
    size_t size = 1024;
    SYSTEM_PROCESS_INFORMATION *spi = malloc(size);
    NTSTATUS status;

    for (;;)
    {
        status = NtQuerySystemInformation(SystemProcessInformation, spi, size, NULL);

        if (status == STATUS_INFO_LENGTH_MISMATCH)
        {
            SYSTEM_PROCESS_INFORMATION *new_spi;

            size *= 2;
            new_spi = realloc(spi, size);
            if (!new_spi)
            {
                free(spi);
                return STATUS_NO_MEMORY;
            }
            spi = new_spi;
            continue;
        }

        if (status == STATUS_SUCCESS)
            *out = spi;
        else
            free(spi);

        return status;
    }
}

static uint32_t next_watchpoint_entry = 0;

static void **get_debug_register_addr(unsigned int idx)
{
    switch (idx)
    {
        case 0: return (void **)&cached_context.Dr0;
        case 1: return (void **)&cached_context.Dr1;
        case 2: return (void **)&cached_context.Dr2;
        case 3: return (void **)&cached_context.Dr3;
        default: return NULL;
    }
}

static NTSTATUS set_watchpoint( void *args )
{
    SYSTEM_PROCESS_INFORMATION *spi, *orig_spi = NULL;
    struct gio_unix_set_watchpoint *data = args;
    uintptr_t dr7_value, dr7_mask;
    void **addr_reg;
    ULONG off = 0;

    if (data->entry == ~0u)
    {
        unsigned int i;

        for (i = 0; i < 4; i++)
        {
            if ((cached_context.Dr7 & (0x3 << 2 * i)) && *get_debug_register_addr(i) == data->addr)
            {
                data->entry = i;
                break;
            }
        }
    }

    if (data->entry == ~0u)
    {
        data->entry = next_watchpoint_entry;
        next_watchpoint_entry = (next_watchpoint_entry + 1) % 4;
    }

    addr_reg = get_debug_register_addr(data->entry);

    cached_context.ContextFlags = CONTEXT_DEBUG_REGISTERS;
    *addr_reg = data->addr;
    dr7_value = 0x1 << (2 * data->entry);
    dr7_value |= (data->condition & 0x3) << (16 + 4 * data->entry);
    dr7_value |= (data->size & 0x3) << (18 + 4 * data->entry);
    dr7_mask = 0x3 << (2 * data->entry);
    dr7_mask |= 0xf << (16 + 4 * data->entry);
    cached_context.Dr7 = (cached_context.Dr7 & ~dr7_mask) | dr7_value;

    if ((data->status = retrieve_system_process_information(&orig_spi)))
        goto error;
    spi = orig_spi;

    do
    {
        unsigned int i;

        spi = (SYSTEM_PROCESS_INFORMATION *)((char *)spi + off);

        if (HandleToULong(spi->UniqueProcessId) != GetCurrentProcessId())
            continue;

        for (i = 0; i < spi->dwThreadCount; i++)
        {
            SYSTEM_THREAD_INFORMATION *sti = &spi->ti[i];

            if ((data->status = set_watchpoints_for_thread(HandleToULong(sti->ClientId.UniqueThread))))
                goto error;
        }
    } while ((off = spi->NextEntryOffset));

error:
    free(orig_spi);

    if (!data->status)
        GTRACE("Set watchpoint entry %d for address %p\n", data->entry, data->addr);
    else
        GERR("Failed to set watchpoint for address %p\n", data->addr);

    return STATUS_SUCCESS;
}

static NTSTATUS dump_hex(void *args)
{
    struct gio_unix_dump_hex *data = args;
    const unsigned bytes_per_line = 16;
    uint64_t i;

    for (i = 0; i < data->size; i++)
    {
        if (i % bytes_per_line == 0)
            GTRACE("\1%016llx:", (unsigned long long)(uintptr_t)data->ptr + i);
        GTRACE(" %02x", (unsigned)(unsigned char)data->ptr[i]);
        if (i % bytes_per_line == bytes_per_line - 1)
            GTRACE("\n");
    }
    if (i % bytes_per_line != 0)
        GTRACE("\n");

    return STATUS_SUCCESS;
}

static NTSTATUS emit_log(void *args)
{
    static const char * const wine_prefix = "/wine/";
    struct gio_unix_emit_log *data = args;
    struct debug_info *info = get_info();
    char *pos = info->output;
    const char *file_suffix;

    if (init_done)
    {
        if (TRACE_ON(timestamp))
        {
            UINT ticks = NtGetTickCount();
            pos += sprintf( pos, "%3u.%03u:", ticks / 1000, ticks % 1000 );
        }
        if (TRACE_ON(microsecs))
        {
            LARGE_INTEGER counter, frequency, microsecs;
            NtQueryPerformanceCounter(&counter, &frequency);
            microsecs.QuadPart = counter.QuadPart / (frequency.QuadPart / 1000000);
            pos += sprintf( pos, "%u.%06u:", (unsigned int)(microsecs.QuadPart / 1000000), (unsigned int)(microsecs.QuadPart % 1000000) );
        }
        if (TRACE_ON(pid)) pos += sprintf( pos, "%04x:", (UINT)GetCurrentProcessId() );
        pos += sprintf( pos, "%04x:", (UINT)GetCurrentThreadId() );
    }
    file_suffix = strstr(data->file, wine_prefix);
    if (file_suffix)
        file_suffix += sizeof(wine_prefix) - 2;
    else
        file_suffix = data->file;
    pos += snprintf( pos, sizeof(info->output) - (pos - info->output), "%s:%s%s:%uG:%p:%s:%d:%s ",
                     data->cls, data->suppress ? "@" : "", data->channel, (unsigned)sizeof(void*),
                     data->retaddr, file_suffix, data->line, data->func );
    info->out_pos = pos - info->output;
    __wine_dbg_output(data->message);

    return STATUS_SUCCESS;
}

static NTSTATUS wine_gio_call_unix( int code, void *args, bool from_win )
{
    switch (code)
    {
        case GIO_UNIX_ECHO: return unix_echo(args);
        case GIO_UNIX_DISASSEMBLE_MEMORY: return disassemble_memory(args);
        case GIO_UNIX_DUMP_SYMBOL: return dump_symbol(args);
        case GIO_UNIX_BACKTRACE: return dump_backtrace(args);
        case GIO_UNIX_BACKTRACE_FROM_CONTEXT: return dump_backtrace_from_context(args);
        case GIO_UNIX_SET_LOADING_WINE_CODE: return set_loading_wine_code(args);
        case GIO_UNIX_WRITE_TO_FILE: return write_to_file(args);
        case GIO_UNIX_SET_WATCHPOINT: return set_watchpoint(args);
        case GIO_UNIX_DUMP_HEX: return dump_hex(args);
        case GIO_UNIX_EMIT_LOG: return emit_log(args);
        case GIO_UNIX_SEND_USR2: return send_usr2(args);
        default:
            GERR("Invalid gio call %d\n", code);
            return STATUS_INVALID_PARAMETER;
    }
}

/***********************************************************************
 *		unixcall_wine_gio_call
 */
NTSTATUS unixcall_wine_gio_call( void *args )
{
    struct wine_gio_call_params *params = args;

    return wine_gio_call_unix( params->code, params->params, true );
}

DECLSPEC_EXPORT NTSTATUS WINAPI __wine_gio_call( int code, void *args )
{
    return wine_gio_call_unix( code, args, false );
}

#ifdef _WIN64
/***********************************************************************
 *		wow64_wine_gio_call
 */
NTSTATUS wow64_wine_gio_call( void *args )
{
    GERR("WoW64 gio call not implemented\n");
    return STATUS_NOT_IMPLEMENTED;
}
#endif

void log_signal( int signal, siginfo_t *siginfo, ucontext_t *ucontext, bool dump_ip )
{
    struct maps_line *maps;
    CONTEXT context = {0};
    uintptr_t ip;

    if (!TRACE_ON(signal))
        return;

    if (!NtCurrentTeb())
    {
        write(2, "signal ", strlen("signal "));
        write(2, sigabbrev(signal), strlen(sigabbrev(signal)));
        write(2, "received but no teb, bye\n", strlen("received but no teb, bye\n"));
        return;
    }
#if defined(__x86_64__)
    ip = ucontext->uc_mcontext.gregs[REG_RIP];
#elif defined(__i386__)
    ip = ucontext->uc_mcontext.gregs[REG_EIP];
#else
#error "Unknown architecture"
#endif

    TRACE_(signal)("\1received %s (%d) at %p: signo %d errno %d code %d pid %d uid %d status %d addr %p is_inside_syscall %u\n",
            sigabbrev(signal), signal, (void *)ip,
            siginfo->si_signo, siginfo->si_errno, siginfo->si_code,
            siginfo->si_pid, siginfo->si_uid, siginfo->si_status, siginfo->si_addr,
            is_inside_syscall(ucontext));

    if (dump_ip && ip)
        __wine_dump_symbol((void*)ip);

    maps = scan_maps();

    unix_backtrace(maps);

    if (is_inside_syscall(ucontext))
        context_from_syscall_frame(&context);
    else
        context_from_ucontext(&context, ucontext);

    win_backtrace_from_context(&context, maps);

    free_maps_lines(maps);
}

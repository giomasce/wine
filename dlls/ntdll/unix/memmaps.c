/*
 * Scan memory map
 *
 * Copyright 2021 Giovanni Mascellani for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
#pragma makedep unix
#endif

#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>

#include "winternl.h"
#include "unix_private.h"
#include "wine/debug.h"

#if 1

WINE_DEFAULT_DEBUG_CHANNEL(memmaps);

void free_maps_lines(struct maps_line *lines)
{
    int i;

    if (!lines)
        return;

    for (i = 0; lines[i].file != NULL; i++) free(lines[i].file);
    free(lines);
}

struct maps_line *scan_maps(void)
{
    struct maps_line *lines = NULL;
    FILE *maps;
    int res, i, c, line_num = 16;
    char *file;
    const char *last_file = "<unnamed>";
    off_t last_size = 0;
    size_t file_len, file_strlen;
    void *from, *to, *off;
    void *last_to = NULL, *last_to_off = NULL;

    maps = fopen("/proc/self/maps", "r");
    if (!maps) return NULL;

    lines = malloc(line_num * sizeof(*lines));
    if (lines == NULL)
    {
        fclose(maps);
        return NULL;
    }

    for (i = 0; ; i++)
    {
        if (i == line_num)
        {
            line_num *= 2;
            lines = realloc(lines, line_num * sizeof(*lines));
            if (lines == NULL)
            {
                // This is leaking the strings
                fclose(maps);
                return NULL;
            }
        }

        res = fscanf(maps, "%p-%p %*c%*c%*c%*c %p %*x:%*x %*u", &from, &to, &off);
        if (res != 3)
        {
            lines[i].file = NULL;
            break;
        }

        while (1)
        {
            c = fgetc(maps);
            if (c == EOF) break;
            if (c == '\n' || !isblank(c))
            {
                ungetc(c, maps);
                break;
            }
        }

        file = NULL;
        file_len = 0;
        getline(&file, &file_len, maps);
        file_strlen = strlen(file);
        if (file_strlen > 0 && file[file_strlen-1] == '\n') file[file_strlen-1] = '\0';
        if (file[0] != '\0')
        {
            if (strcmp(last_file, file) != 0)
            {
                struct stat st;
                if (stat(file, &st) == 0)
                    last_size = st.st_size;
                else
                    last_size = 0;
            }
        }
        else
        {
            free(file);
            /* If we're still in the previous file's footprint, we
               presume that the map actually belongs to the same file,
               but was somehow modified. */
            if (from == last_to && (char*)last_to_off + ((char*)to - (char*)from) <= (char*)(uintptr_t)last_size)
            {
                file = strdup(last_file);
                off = last_to_off;
            }
            else
                file = strdup("<unnamed>");
        }

        TRACE("%p-%p file %s offset %p\n", from, to, file, off);

        lines[i].from = from;
        lines[i].to = to;
        lines[i].off = off;
        lines[i].file = file;
        last_to = to;
        last_to_off = (char*)off + ((char*)to - (char*)from);
        last_file = file;
    }
    fclose(maps);

    return lines;
}

struct maps_line *find_map(struct maps_line *maps, void *addr)
{
    for (; maps->file; maps++)
    {
        if (maps->from <= addr && addr < maps->to)
            return maps;
    }

    return NULL;
}

#else

void free_maps_lines(struct maps_line *lines)
{
}

struct maps_line *scan_maps(void)
{
    return NULL;
}

struct maps_line *find_map(struct maps_line *maps, void *addr)
{
    return NULL;
}

#endif
